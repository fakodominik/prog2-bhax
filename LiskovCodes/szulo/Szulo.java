package szulo;

public class Szulo	//anyavallalat
{
	protected String name;
	protected int numberOfEmployees;

	public Szulo(String name,int num)
	{
		this.name = name;
		this.numberOfEmployees = num;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}
	public int getNumberOfWorkers()
	{
		return numberOfEmployees;
	}

}