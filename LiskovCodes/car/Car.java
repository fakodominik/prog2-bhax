package car;

public class Car extends Vehicle
{
	public Car()
	{
		System.out.println("Car's constructor");
	}

	@Override
	public void start()
	{
		System.out.println("Car's start()");
	}
}