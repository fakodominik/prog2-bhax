package car;

public class Main
{
	public static void main(String[] args)
	{
		Vehicle firstVehicle = new Supercar();
		firstVehicle.start();
		System.out.println(firstVehicle instanceof Car);

		Car secondVehicle = (Car) firstVehicle;
		secondVehicle.start();
		System.out.println(secondVehicle instanceof Supercar);

		//Supercar thirdVehicle = new Vehicle(); -- hiba
		Supercar thirdVehicle = new Supercar();
		thirdVehicle.start();
	}
}