package binfa;

import java.io.FileWriter;
import java.io.IOException;

public class LZWBinfa
{
    public Csomopont root;
    public Csomopont fa;
    public int melyseg;
    public double szoras;
    public double atlag;
    public static String fileName = Main.fileName;
    public static FileWriter myWriter = Main.myWriter;
    

    public LZWBinfa()
    {
        root = new Csomopont();
        fa = root;
    }
    
    public void usage()
    {
        System.out.println("Használat : java Main <inputFile> <outputFile>");
    }
    
    public void kiir() throws IOException
    {
        melyseg = 0;
        kiir(root);
    }
    
    public void pushToTree(int num)
    {
        if(num == 0)
        {
            if(fa.getNullasGyermek() == null)
            {
                Csomopont uj = new Csomopont(0);
                fa.setNullasGyermek(uj);
                fa = root;
            }else
            {
                fa = fa.getNullasGyermek();
            }
        }else
        {
            if(fa.getEgyesGyermek() == null)
            {
                Csomopont uj = new Csomopont(1);
                fa.setEgyesGyermek(uj);
                fa = root;
            }else
            {
                fa = fa.getEgyesGyermek();
            }
        }
    }
    
    public void kiir(Csomopont elem) throws IOException
    {
        if(elem != null)
        {
            ++melyseg;
            
            kiir(elem.getEgyesGyermek());
            
            kiir(elem.getNullasGyermek());
            
            for(int i = 0;i< melyseg; i++)
            {
                myWriter.write("---");
            }
            myWriter.write(elem.getBetu() + "(" + Integer.toString(melyseg-1) +")" + "\n");
            --melyseg;
        }
        
        
        myWriter.close();
    }
}