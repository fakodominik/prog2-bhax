package binfa;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.Arrays;
import binfa.LZWBinfa;

public class Main
{
    public static LZWBinfa binfa = new LZWBinfa();
    public static String fileName;
    public static FileWriter myWriter;
    public static Scanner myReader;
    private static char givenChar;
    
    public static void main(String[] args) throws IOException
    {

        if(args.length != 2)
        {
            binfa.usage();
        }
        
        try
        {
            fileName = args[1];
            myWriter = new FileWriter(fileName);
            myReader = new Scanner(args[0]);
        }catch(IOException e)
        {
            binfa.usage();
            System.out.println("A hiba : " + e.toString() + " , azaz nincs ilyen fájl!");
        }
        
        while(myReader.hasNextLine())
        {
            givenChar = (char) myReader.next().charAt(0);
            if(givenChar == '1')
            {
                binfa.pushToTree(1);
            }
            if(givenChar == '0')
            {
                binfa.pushToTree(0);
            }
        }
        
        
        
        binfa.kiir();
        
        myReader.close();
        myWriter.close();
    }
}