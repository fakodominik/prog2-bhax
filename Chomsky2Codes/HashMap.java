package hu.unideb.prog2;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class HashMap<K,V> implements Map<K,V>{

	private Node<K,V> root;
	
	@Override
	public int size() {
		if(root == null)
		{
			return 0;
		}else
		{
		int count = 0;
		Node<K,V> current = root;
		while(current != null)
		{
			count++;
			current = current.getNext();
		}
		return count;
		}
	}

	@Override
	public boolean isEmpty() {
		if(this.size() == 0)
		{
			return true;
		}else
		{
			return false;
		}
	}

	@Override
	public boolean containsKey(Object k) {
		int key = k.hasCode();
		Node<K,V> current = root;
		for(int i =0;i<this.size();i++)
		{
			if(current.getKey() == key)
			{
				return true;
			}
			current = current.getNext();
		}
		return false;
	}

	@Override
	public boolean containsValue(Object value) {
		Node<K,V> current = root;
		for(int i =0;i<this.size();i++)
		{
			if(current.getValue() == value)
			{
				return true;
			}
			current = current.getNext();
		}
		return false;

	}

	@Override
	public V get(Object k) {
		int key = k.hasCode();
		Node<K,V> current = root;
		for(int i =0;i<this.size();i++)
		{
			if(current.getKey() == key)
			{
				return current.getValue();
			}
			current = current.getNext();
		}
		return null;

	}

	@Override
	public V put(K k, V value) {
		int key = k.hasCode();
		if(size() == 0)
		{
			root = new Node<K,V>(key,value,null);
		}else
		{
			Node<K,V> current = root.getNext();
			Node<K,V> before = root;
			if(before.getKey() == key)
			{
				return null;
			}
			while(current != null)
			{
				if(current.getKey() == key)
				{
					return null;
				}
				before = current;
				current = current.getNext();
			}
				current = new Node<K,V>(key,value,null);
				before.setNext(current);
		}
		return null;
	}

	@Override
	public V remove(Object k) {
		int key = k.hasCode();
		
		if(root == null)
		{
			return null;
		}else
		{
			Node<K,V> current = root.getNext();
			Node<K,V> before = root;
			if(before.getKey()==key)
			{
				root = current;
				before = null;
				return null;
			}else
			{
				for(int i=0;i<size();i++)
				{
					if(current.getKey() == key)
					{
						before.setNext(current.getNext());
						current = null;
						return null;
					}
					before = current;
					current = current.getNext();
				}
			}
		}
		return null;
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
			m.forEach(this::put);
	}

	@Override
	public void clear() {
		
		if(root != null)
		{
			Node<K,V> current = root.getNext();
			Node<K,V> before = root;
			while(current != null)
			{
				before = null;
				before = current;
				current = current.getNext();
			}
			before = null;
			root = before;
		}
	}

	@Override
	public Set<K> keySet() {
		Set<K> set = new HashSet<>();
		Node<K,V> current = root;
		while(current != null)
		{
			set.add(current.getKey());
			current = current.getNext();
		}
		return set;
	}

	@Override
	public Collection<V> values() {
		Collection<V> collection = new ArrayList<V>();
		Node<K,V> current = root;
		while(current != null)
		{
			collection.add(current.getValue());
			current = current.getNext();
		}
		return collection;
	}

	@Override
	public Set<Entry<K, V>> entrySet() {
		Set<Entry<K,V>> set = new HashSet<>();
		Entry<K,V> entry;
		Node<K,V> current = root;
		while(current != null)
		{
			entry = new AbstractMap.SimpleEntry<K, V>(current.getKey(),current.getValue());
			set.add(entry);
			current = current.getNext();
		}
		return set;
	}

	public class Node<K,V>
	{
		private V value;
		private K key;
		private Node<K,V> next;
		
		
		public Node(K key,V value, Node<K, V> next) {
			super();
			this.value = value;
			this.key = key;
			this.next = next;
		}
		public V getValue() {
			return value;
		}
		public K getKey() {
			return key;
		}
		public Node<K, V> getNext() {
			return next;
		}
		public void setNext(Node<K, V> next) {
			this.next = next;
		}
		
		
	}

}
