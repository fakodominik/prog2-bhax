package storage;

import java.util.Random;

public class Main
{
	public static void main(String[] args)
	{
		IntegerStorage storage = new IntegerStorage(10);
		Random rand = new Random();
		for(int i=0;i<10;i++)
		{
			storage.add(rand.nextInt(100));
		}
		System.out.println("The number 5 is in the array? :" + storage.contains(5));
	}
}