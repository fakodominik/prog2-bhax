import javax.swing.*;

public class Fullscreen
{
	private JFrame frame;

	public static void main(String[] args)
	{
		Fullscreen f = new Fullscreen();
		f.run();
	}

	public Fullscreen()
	{
		frame = new JFrame("Empty Title!");
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	}

	public void run()
	{
		frame.setVisible(true);
	}
}