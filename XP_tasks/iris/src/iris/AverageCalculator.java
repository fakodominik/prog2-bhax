package iris;

import java.util.ArrayList;

/**
 * This class calculates the average of every data given in the {@link Iris} object.
 */
public class AverageCalculator {

	/**
	 * Sets the average sepal length in all of the given species
	 * @param species : The database that contains all of the species
	 */
	public static void avgsLength(ArrayList<IrisSpecies> species)
	{
		//Keep track of the sum in the given species
		double sum = 0;
				
		//Go through of all the species
		for(IrisSpecies s : species)
		{
			//Count the sum of sepal length
			for(int i = 0;i<s.getsLengthValues().size();i++)
			{
				sum += s.getsLengthValues().get(i);
			}
			
			//Sets the average sepal length
			s.setAvgsLength(sum / s.getNumber());
			
			//Sets the sum to 0 to use it on the following species
			sum = 0;
		}
	}
	/**
	 * Sets the average sepal width in all of the given species
	 * @param species : The database that contains all of the species
	 */
	public static void avgsWidth(ArrayList<IrisSpecies> species)
	{
		//Keep track of the sum in the given species
		double sum = 0;
				
		//Go through of all the species
		for(IrisSpecies s : species)
		{
			//Count the sum of sepal width
			for(int i = 0;i<s.getsWidthValues().size();i++)
			{
				sum += s.getsWidthValues().get(i);
			}
			
			//Sets the average sepal length
			s.setAvgsWidth(sum / s.getNumber());
			
			//Sets the sum to 0 to use it on the following species
			sum = 0;
		}
	}
	/**
	 * Sets the average petal length in all of the given species
	 * @param species : The database that contains all of the species
	 */
	public static void avgpLength(ArrayList<IrisSpecies> species)
	{
		//Keep track of the sum in the given species
		double sum = 0;
				
		//Go through of all the species
		for(IrisSpecies s : species)
		{
			//Count the sum of petal length
			for(int i = 0;i<s.getpLengthValues().size();i++)
			{
				sum += s.getpLengthValues().get(i);
			}
			
			//Sets the average sepal length
			s.setAvgpLength(sum / s.getNumber());
			
			//Sets the sum to 0 to use it on the following species
			sum = 0;
		}
	}
	/**
	 * Sets the average petal width in all of the given species
	 * @param species : The database that contains all of the species
	 */
	public static void avgpWidth(ArrayList<IrisSpecies> species)
	{
		//Keep track of the sum in the given species
		double sum = 0;
				
		//Go through of all the species
		for(IrisSpecies s : species)
		{
			//Count the sum of petal width
			for(int i = 0;i<s.getpWidthValues().size();i++)
			{
				sum += s.getpWidthValues().get(i);
			}
			
			//Sets the average sepal length
			s.setAvgpWidth(sum / s.getNumber());
			
			//Sets the sum to 0 to use it on the following species
			sum = 0;
		}
	}
	/**
	 * This returns a list of all species categorized by the name of species.
	 * @param list : The list that contains all of the data, including the names of the species.
	 * @return species : The list of instances filtered by the occurred different names in the database.
	 */
	public static ArrayList<IrisSpecies> getSpecies(ArrayList<Iris> list)
	{
		//Goes through all the elements and add the missing names to the names list
		ArrayList<String> names = new ArrayList<>();
		ArrayList<IrisSpecies> species = new ArrayList<>();
		
		//Create instances by getting their names
		for(Iris i : list)
		{
			if(!names.contains(i.getName()))
			{
				IrisSpecies irisSpecies = new IrisSpecies(i.getName());
				species.add(irisSpecies);
				names.add(i.getName());
			}
			
		}
		
		//Counting their occurrence
		for(Iris i : list)
		{
			for(IrisSpecies s : species)
			{
				if(i.getName().equals(s.getName()))
				{
					s.numberIncrement();
				}
			}
		}
		
		return species;
	}
	
}
