package iris;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * This class has one purpose, the communicate with the user, write out the output to
 * different output streams.
 */
public class WriteToOutput {

	/**
	 * Write the average results to the console output.
	 * @param species : The list that contains every Iris species.
	 */
	public static void avgWriteToConsole(ArrayList<IrisSpecies> species)
	{
		//Formatting the output
		System.out.println("\t::Average values::");
		for(IrisSpecies i : species)
		{
			System.out.println(i.getName() + " : ");
			System.out.println("\tAverage sepal length : " + i.getAvgsLength());
			System.out.println("\tAverage sepal width : " + i.getAvgsWidth());
			System.out.println("\tAverage petal length : " + i.getAvgpLength());
			System.out.println("\tAverage petal width : " + i.getAvgpWidth());
		}
	}
	/**
	 * Write out the average results to the given file.
	 * @param species : The list that contains every Iris species.
	 * @param fileName : The name of the file that we are trying to write to.
	 * @throws IOException : In case of something bad happen to the file.
	 */
	public static void avgWriteToFile(ArrayList<IrisSpecies> species, String fileName) throws IOException
	{
		//Required objects
		File file = new File(fileName);
		PrintWriter writer = new PrintWriter(file);
		
		//Formatting the output
		writer.append("\t::Average values::\n");
		for(IrisSpecies i : species)
		{
			writer.append(i.getName() + " : \n");
			writer.append("\tAverage sepal length : " + i.getAvgsLength() + "\n");
			writer.append("\tAverage sepal width : " + i.getAvgsWidth()+ "\n");
			writer.append("\tAverage petal length : " + i.getAvgpLength()+ "\n");
			writer.append("\tAverage petal width : " + i.getAvgpWidth()+ "\n");
		}
		
		writer.close();
		
	}
	/**
	 * Write the median results to the console output.
	 * @param species : The list that contains every Iris species.
	 */
	public static void medianWriteToConsole(ArrayList<IrisSpecies> species)
	{
		//Formatting the output
		System.out.println("\n\t::Median values::");
		for(IrisSpecies i : species)
		{
			System.out.println(i.getName() + " : ");
			System.out.println("\tMedian sepal length : " + i.getMediansLength());
			System.out.println("\tMedian sepal width : " + i.getMediansWidth());
			System.out.println("\tMedian petal length : " + i.getMedianpLength());
			System.out.println("\tMedian petal width : " + i.getMedianpWidth());
		}
	}
	/**
	 * Append the output median results to the given file;
	 * @param species : The list that contains every Iris species.
	 * @param fileName : The name of the file that we are trying to write to.
	 * @throws IOException : In case of something bad happen to the file.
	 */
	public static void medianWriteToFile(ArrayList<IrisSpecies> species, String fileName) throws IOException
	{
		//Required objects
		FileWriter file = new FileWriter(fileName,true);
		PrintWriter writer = new PrintWriter(file);
		
		//Formatting the output
		writer.append("\n\n\t::Median values::\n");
		for(IrisSpecies i : species)
		{
			writer.append(i.getName() + " : \n");
			writer.append("\tMedian sepal length : " + i.getMediansLength() + "\n");
			writer.append("\tMedian sepal width : " + i.getMediansWidth()+ "\n");
			writer.append("\tMedian petal length : " + i.getMedianpLength()+ "\n");
			writer.append("\tMedian petal width : " + i.getMedianpWidth()+ "\n");
		}
		writer.close();
		
	}
}
