package iris;

/**
 *The class that represents a single line, splitted into sections, from the given file.
 */
public class Iris {

	private double sLength;	//sepalLength
	private double sWidth;	//sepalWidth
	private double pLength;	//petalLength
	private double pWidth;	//petalWidth
	private String name;	//name of the species	
	
	
	//Getters and setters
	public double getsLength() {
		return sLength;
	}
	public void setsLength(double sLength) {
		this.sLength = sLength;
	}
	public double getsWidth() {
		return sWidth;
	}
	public void setsWidth(double sWidth) {
		this.sWidth = sWidth;
	}
	public double getpLength() {
		return pLength;
	}
	public void setpLength(double pLength) {
		this.pLength = pLength;
	}
	public double getpWidth() {
		return pWidth;
	}
	public void setpWidth(double pWidth) {
		this.pWidth = pWidth;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
