package iris;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * A class that has only one purpose, to read from a given file and create an {@link Iris}
 * object for every line while tokenizing the current line.
 */
public class Tokenizer {

	/**
	 *Reads from a file and returns an {@link ArrayList} object.
	 *@param fileName : The name of the file from where we try to read.
	 *@return list : The array that contains all data read from the file.
	 */
	public static ArrayList<Iris> readFromFile(String fileName) throws IOException
	{
		//Required objects
		File file = new File(fileName);
		Scanner scanner = new Scanner(file);
		ArrayList<Iris> list = new ArrayList<>();
		
		//A single line
		String line;
		
		//A line divided into parts
		String[] parts;
		
		//Reading and and splitting while there is no line left. Adding the the list
		while(scanner.hasNextLine())
		{
			Iris temp = new Iris();
			line = scanner.nextLine();
			parts = line.split(",");
			
			temp.setsLength(Double.parseDouble(parts[0]));
			temp.setsWidth(Double.parseDouble(parts[1]));
			temp.setpLength(Double.parseDouble(parts[2]));
			temp.setpWidth(Double.parseDouble(parts[3]));
			temp.setName(parts[4]);
			
			list.add(temp);
		}
		scanner.close();
		
		return list;
	}
	
	/**
	 * This loads the values into another list containing the different species.
	 * @param list : Containing the raw database
	 * @param species : Containing the different kind of species
	 */
	public static void loadUpValues(ArrayList<Iris> list, ArrayList<IrisSpecies> species)
	{
		for(Iris i : list)
		{
			for(IrisSpecies s : species)
			{
				if(i.getName().equals(s.getName()))
				{
					s.addsLength(i.getsLength());
					s.addsWidth(i.getsWidth());
					s.addpLength(i.getpLength());
					s.addpWidth(i.getpWidth());
				}
			}
		}
	}
	
}
