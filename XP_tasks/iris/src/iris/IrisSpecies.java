package iris;

import java.util.ArrayList;

/**
 * This class represent a single species of all the {@link Iris} species.
 */
public class IrisSpecies {

	private String name;		//Name of the species
	private int number;			//Occurrence in the file
	
	//Average values
	private double avgsLength;	//Average sepal length
	private double avgsWidth;	//average sepal width
	private double avgpLength;	//average petal length
	private double avgpWidth;	//average petal width
	
	//Median values
	private double mediansLength;	//Median sepal length
	private double mediansWidth;	//Median sepal width
	private double medianpLength;	//Median petal length
	private double medianpWidth;	//Median petal width
	
	//Lists containing the different values
	private ArrayList<Double> sLengthValues;
	private ArrayList<Double> sWidthValues;
	private ArrayList<Double> pLengthValues;
	private ArrayList<Double> pWidthValues;
	
	/**
	 * Creates an instance of a species given the correct name.
	 * @param name : The name of the current IrisSpecies.
	 */
	public IrisSpecies(String name) {
		this.number = 0;
		this.name = name;
		this.avgsLength = 0;
		this.avgsWidth = 0;
		this.avgpLength = 0;
		this.avgpWidth = 0;
		this.mediansLength = 0;
		this.mediansWidth = 0;
		this.medianpLength = 0;
		this.medianpWidth = 0;
		this.sLengthValues = new ArrayList<>();
		this.sWidthValues = new ArrayList<>();
		this.pLengthValues = new ArrayList<>();
		this.pWidthValues = new ArrayList<>();
	}
	
	//The way to increase the number of the occurrence
	public void numberIncrement()
	{
		this.number++;
	}
	
	//Only getter for the number and name field
	public int getNumber()
	{
		return this.number;
	}
	public String getName()
	{
		return this.name;
	}
	
	//Getters and setters except for name and number
	public double getAvgsLength() {
		return avgsLength;
	}
	public void setAvgsLength(double avgsLength) {
		this.avgsLength = avgsLength;
	}
	public double getAvgsWidth() {
		return avgsWidth;
	}
	public void setAvgsWidth(double avgsWidth) {
		this.avgsWidth = avgsWidth;
	}
	public double getAvgpLength() {
		return avgpLength;
	}
	public void setAvgpLength(double avgpLength) {
		this.avgpLength = avgpLength;
	}
	public double getAvgpWidth() {
		return avgpWidth;
	}
	public void setAvgpWidth(double avgpWidth) {
		this.avgpWidth = avgpWidth;
	}

	public double getMediansLength() {
		return mediansLength;
	}

	public void setMediansLength(double mediansLength) {
		this.mediansLength = mediansLength;
	}

	public double getMediansWidth() {
		return mediansWidth;
	}

	public void setMediansWidth(double mediansWidth) {
		this.mediansWidth = mediansWidth;
	}

	public double getMedianpLength() {
		return medianpLength;
	}

	public void setMedianpLength(double medianpLength) {
		this.medianpLength = medianpLength;
	}

	public double getMedianpWidth() {
		return medianpWidth;
	}

	public void setMedianpWidth(double medianpWidth) {
		this.medianpWidth = medianpWidth;
	}

	public ArrayList<Double> getsLengthValues() {
		return sLengthValues;
	}

	public ArrayList<Double> getsWidthValues() {
		return sWidthValues;
	}

	public ArrayList<Double> getpLengthValues() {
		return pLengthValues;
	}

	public ArrayList<Double> getpWidthValues() {
		return pWidthValues;
	}
	
	public void addsLength(Double num)
	{
		this.sLengthValues.add(num);
	}
	public void addsWidth(Double num)
	{
		this.sWidthValues.add(num);
	}
	public void addpLength(Double num)
	{
		this.pLengthValues.add(num);
	}
	public void addpWidth(Double num)
	{
		this.pWidthValues.add(num);
	}
}
