package iris;

import java.io.IOException;
import java.util.ArrayList;

/**
 * The class that start the main() method.
 */

public class Main {

	/**
	 * The start of the program.
	 * @param args : Arguments got from the console.
	 * @throws Exception : In case of anything bad happens.
	 */
	public static void main(String[] args) throws Exception{
		//In case of no file given
		if(args.length == 0)
		{
			throw new Exception("No file given!");
		}else if(args.length == 1)	
		{
			//Given exactly 1 argument leads to write out to console
			ArrayList<Iris> myList = new ArrayList<>();
			ArrayList<IrisSpecies> species = new ArrayList<>();
			try
			{
				myList = Tokenizer.readFromFile(args[0]);
				
			}catch(IOException e)
			{
				throw new Exception("Wrong file given!");
			}
			
			//Getting the different species
			species = AverageCalculator.getSpecies(myList);
			
			//Loading and setting the values
			loadAndSetValues(myList,species);
			
			//Printing out the average values of the different species
			WriteToOutput.avgWriteToConsole(species);
			
			//Printing out the median values of the different species
			WriteToOutput.medianWriteToConsole(species);
			
			
		}else if(args.length == 2)
		{
			ArrayList<Iris> myList = new ArrayList<>();
			ArrayList<IrisSpecies> species = new ArrayList<>();
			try
			{
				myList = Tokenizer.readFromFile(args[0]);
				
			}catch(IOException e)
			{
				throw new Exception("Wrong file given!");
			}
			
			//Getting the different species
			species = AverageCalculator.getSpecies(myList);
			
			//Loading and setting the values
			loadAndSetValues(myList,species);
			
			try
			{
				//Printing out the results
				WriteToOutput.avgWriteToFile(species,args[1]);
				WriteToOutput.medianWriteToFile(species, args[1]);
				
			}catch(IOException e)
			{
				throw new Exception(e.toString());
			}
			
		}else
		{
			throw new Exception("Too many arguments given!");
		}
	}
	/**
	 * This fills up the value-empty species list with data given from the list database.
	 * @param myList : The list containing the raw database.
	 * @param species : The list containing the different kind of species.
	 */
	private static void loadAndSetValues(ArrayList<Iris> myList, ArrayList<IrisSpecies> species)
	{
		//Loading the values to our new list
		Tokenizer.loadUpValues(myList, species);
		
		//Getting the average values
		AverageCalculator.avgsLength(species);
		AverageCalculator.avgpLength(species);
		AverageCalculator.avgsWidth(species);
		AverageCalculator.avgpWidth(species);
		
		//Getting the median values
		MedianCalculator.mediansLenght(species);
		MedianCalculator.mediansWidth(species);
		MedianCalculator.medianpLenght(species);
		MedianCalculator.medianpWidth(species);
	}

}
