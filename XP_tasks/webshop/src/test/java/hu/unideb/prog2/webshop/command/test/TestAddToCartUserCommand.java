package hu.unideb.prog2.webshop.command.test;

import org.junit.Assert;
import org.junit.Test;

import hu.unideb.prog2.webshop.command.user.impl.AddToCartUserCommand;
import hu.unideb.prog2.webshop.data.DataBase;
import hu.unideb.prog2.webshop.model.Cart;
import hu.unideb.prog2.webshop.model.Product;
import hu.unideb.prog2.webshop.model.ProductType;

public class TestAddToCartUserCommand {

private AddToCartUserCommand command = new AddToCartUserCommand(null,null,null);
	
	@Test
	public void testWrongProcessShouldReturnAText()
	{
		//Given
		String expected = "Need parameters!";
		
		//When
		String result = command.process(null);
		
		//Then
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void testRightProcessShouldAddElementToTheCart()
	{
		//Given
		String[] parameters = {"5"};
		DataBase database = new DataBase();
		ProductType pt = new ProductType("a","a","a",1);
		Product p = new Product(pt,5);
		database.addProduct(p,5);
		int expected = 1;
				
		//When
		command.process(database,parameters);
		int result = Cart.getCart().size();
				
		//Then
		Assert.assertEquals(expected, result);
	}
	
}
