package hu.unideb.prog2.webshop.command.test;

import org.junit.Assert;
import org.junit.Test;

import hu.unideb.prog2.webshop.command.impl.ListProductCommand;
import hu.unideb.prog2.webshop.data.DataBase;
import hu.unideb.prog2.webshop.model.Product;
import hu.unideb.prog2.webshop.model.ProductType;

public class TestListProductCommand {

	private ListProductCommand command = new ListProductCommand(null,null,null);
	
	@Test
	public void testWrongProcessShouldReturnAText()
	{
		//Given
		String expected = "No need for parameters!";
		
		//When
		String result = command.process(null,null);
		
		//Then
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void testRightProcessShouldListAllElementsInTheDatabse()
	{
		//Given
		DataBase database = new DataBase();
		ProductType pt = new ProductType("a","a","a",1);
		Product p = new Product(pt,5);
		database.addProduct(p,5);
		String expected = p.toString() + "\n";
				
		//When
		String result = command.process(database);
				
		//Then
		Assert.assertEquals(expected, result);
	}
}
