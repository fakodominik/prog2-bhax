package hu.unideb.prog2.webshop.command.test;

import org.junit.Test;

import hu.unideb.prog2.webshop.command.impl.ImportProductTypeCommand;
import hu.unideb.prog2.webshop.data.DataBase;

import org.junit.Assert;

public class TestImportProductTypeCommand {

	private ImportProductTypeCommand command = new ImportProductTypeCommand(null,null,null);
	
	@Test
	public void testWrongProcessShouldReturnAText()
	{
		//Given
		String expected = "Need file name as parameter!";
		
		//When
		String result = command.process(null);
		
		//Then
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void testRightProcessShouldAddEveryElementFromTheFileToTheDatabase()
	{
		//Given
		DataBase database = new DataBase();
		String[] parameters = {"src/test/resources/testProductType.csv"};
		int expected = 1;
		
		//When
		command.process(database, parameters);
		int result = database.getProductTypeList().size();
		
		//Then
		Assert.assertEquals(expected, result);
	}
	
}
