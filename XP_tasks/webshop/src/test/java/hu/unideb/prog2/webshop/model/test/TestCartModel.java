package hu.unideb.prog2.webshop.model.test;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.Assert;

import hu.unideb.prog2.webshop.model.Cart;
import hu.unideb.prog2.webshop.model.Product;
import hu.unideb.prog2.webshop.model.ProductType;

public class TestCartModel {

	@Test
	public void testAddToCartShouldAddElementToCartWhenGivenRightProducts() throws Exception{
		//Given
		Cart.emptyCart();
		Cart.addToCart(new Product(new ProductType("smaple","sample","sample",343),23434));
		ArrayList<Product> cart = Cart.getCart();
		int expected = 1;
		
		//When
		int result = cart.size();
		
		//Then
		Assert.assertEquals(expected, result);
		
	}
	
	@Test
	public void testListItemsShouldListTheItemsInTheCart()
	{
		//Given
		Product pt = new Product(new ProductType("smaple","sample","sample",343),23434);
		Cart.addToCart(pt);
		String expected = pt.toString() + "\n";
		
		//When
		String result = Cart.listItems();
		
		//Then
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void testEmptyCartShouldClearAllElementsInTheCart()
	{
		//Given
		Cart.addToCart(new Product(new ProductType("smaple","sample","sample",343),23434));
		ArrayList<Product> cart = Cart.getCart();
		int expected = 0;
		
		//When
		Cart.emptyCart();
		int result = cart.size();
		
		//Then
		Assert.assertEquals(expected, result);
	}
	
	//There is also one getter in Cart
}
