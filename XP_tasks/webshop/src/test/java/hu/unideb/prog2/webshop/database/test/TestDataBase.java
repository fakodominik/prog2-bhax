package hu.unideb.prog2.webshop.database.test;

import org.junit.Test;

import hu.unideb.prog2.webshop.data.DataBase;
import hu.unideb.prog2.webshop.model.Product;
import hu.unideb.prog2.webshop.model.ProductType;

import org.junit.Assert;

public class TestDataBase {

	private DataBase database = new DataBase();
	
	@Test
	public void testAddProductShouldAddProductToProductHashMap()
	{
		//Given
		int expected = 1;
		
		//When
		database.addProduct(new Product(new ProductType("a","a","a",1),1), 1);
		int result = database.getProductList().size();
		
		//Then
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void testAddProductTypeShouldAddProductTypeToProductTypeHashMap()
	{
		//Given
		int expected = 1;
		
		//When
		database.addProductType(new ProductType("a","a","a",1));
		int result = database.getProductTypeList().size();
		
		//Then
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void testGetProductShouldReturnTheProductFromTheProductHashMapByTheGivenKey()
	{
		//Given
		Product p = new Product(new ProductType("a","a","a",1),1);
		Product expected = p;
		database.addProduct(p, 1);
		
		//When
		Product result = database.getProduct(1);
		
		//Then
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void testGetProductTypeShouldReturnTheProductTypeFromTheProductTypeHashMapByTheGivenKey()
	{
		//Given
		ProductType pt = new ProductType("a","a","a",1);
		ProductType expected = pt;
		database.addProductType(pt);
		
		//When
		ProductType result = database.getProductType(0);
		
		//Then
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void testRemoveProductShoouldRemoveAProductFromTheProductHashMapByTheGivenKey()
	{
		//Given
		int expected = 0;
		database.addProduct(new Product(new ProductType("a","a","a",1),1),1);
		
		//When
		database.removeProduct(1);
		int result = database.getProductList().size();
		
		//Then
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void testRemoveProductTypeShoouldRemoveAProductTypeFromTheProductTypeHashMapByTheGivenKey()
	{
		//Given
		int expected = 0;
		database.addProductType(new ProductType("a","a","a",1));
		
		//When
		database.removeProductType(0);
		int result = database.getProductTypeList().size();
		
		//Then
		Assert.assertEquals(expected, result);
	}
	
}
