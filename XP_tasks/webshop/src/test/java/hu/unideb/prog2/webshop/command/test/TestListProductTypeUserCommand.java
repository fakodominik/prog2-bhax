package hu.unideb.prog2.webshop.command.test;

import org.junit.Assert;
import org.junit.Test;

import hu.unideb.prog2.webshop.command.user.impl.ListProductTypeUserCommand;
import hu.unideb.prog2.webshop.data.DataBase;
import hu.unideb.prog2.webshop.model.ProductType;

public class TestListProductTypeUserCommand {

private ListProductTypeUserCommand command = new ListProductTypeUserCommand(null,null,null);
	
	@Test
	public void testWrongProcessShouldReturnAText()
	{
		//Given
		String expected = "No need for parameters!";
		
		//When
		String result = command.process(null,null);
		
		//Then
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void testRightProcessShouldListEveryProductTypeInTheDatabase()
	{
		//Given
		DataBase database = new DataBase();
		ProductType pt = new ProductType("a","a","a",1);
		database.addProductType(pt);
		String expected = pt.toString() + "\n";
		
		//When
		String result = command.process(database);
				
		//Then
		Assert.assertEquals(expected, result);
	}
	
}
