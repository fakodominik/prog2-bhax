package hu.unideb.prog2.webshop.command.test;

import java.util.Set;

import org.junit.Test;

import hu.unideb.prog2.webshop.command.Command;
import hu.unideb.prog2.webshop.command.CommandBuilder;
import org.junit.Assert;

public class TestCommandBuilder {

	@Test
	public void testBuildCommndsShouldAddEveryKindOfCommandToTheSet()
	{
		//Given
		int expected = 12; //The current number of implemented commands
		
		//When
		Set<Command> commands = CommandBuilder.buildCommands();
		int result = commands.size();
		
		//Then
		Assert.assertEquals(expected, result);
	}
	
}
