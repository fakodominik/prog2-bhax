package hu.unideb.prog2.webshop.command;

import java.util.HashSet;
import java.util.Set;

import hu.unideb.prog2.webshop.command.impl.CreateProductCommand;
import hu.unideb.prog2.webshop.command.impl.CreateProductTypeCommand;
import hu.unideb.prog2.webshop.command.impl.ImportProductCommand;
import hu.unideb.prog2.webshop.command.impl.ImportProductTypeCommand;
import hu.unideb.prog2.webshop.command.impl.ListProductCommand;
import hu.unideb.prog2.webshop.command.impl.ListProductTypeCommand;
import hu.unideb.prog2.webshop.command.impl.RemoveProductCommand;
import hu.unideb.prog2.webshop.command.impl.RemoveProductTypeCommand;
import hu.unideb.prog2.webshop.command.user.impl.AddToCartUserCommand;
import hu.unideb.prog2.webshop.command.user.impl.BuyProductUserCommand;
import hu.unideb.prog2.webshop.command.user.impl.ListCartElementsUserCommand;
import hu.unideb.prog2.webshop.command.user.impl.ListProductTypeUserCommand;

public class CommandBuilder {

	public static Set<Command> buildCommands()
	{
		Set<Command> commands = new HashSet<>();
		commands.add(new CreateProductCommand("admin","product","create"));
		commands.add(new CreateProductTypeCommand("admin","product_type","create"));
		commands.add(new RemoveProductCommand("admin","product","remove"));
		commands.add(new RemoveProductTypeCommand("admin","product_type","remove"));
		commands.add(new ImportProductCommand("admin","product","import"));
		commands.add(new ImportProductTypeCommand("admin","product_type","import"));
		commands.add(new ListProductCommand("admin","product","list"));
		commands.add(new ListProductTypeCommand("admin","product_type","list"));
		commands.add(new ListProductTypeUserCommand("user","product_type","list"));
		commands.add(new AddToCartUserCommand("user","cart","add"));
		commands.add(new BuyProductUserCommand("user","cart","buy"));
		commands.add(new ListCartElementsUserCommand("user","cart","list"));
		
		return commands;
	}
	
}
