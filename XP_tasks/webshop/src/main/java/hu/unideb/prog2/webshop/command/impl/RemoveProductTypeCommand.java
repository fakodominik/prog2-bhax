package hu.unideb.prog2.webshop.command.impl;

import hu.unideb.prog2.webshop.command.AbstractCommand;
import hu.unideb.prog2.webshop.data.DataBase;

public class RemoveProductTypeCommand extends AbstractCommand{

	public RemoveProductTypeCommand(String userType, String entityType, String action) {
		super(userType, entityType, action);
	}

	@Override
	public String process(DataBase database) {
		return "Need parameters!";
	}

	@Override
	public String process(DataBase database, String[] parameters) {
		database.removeProductType(Integer.parseInt(parameters[0]));
		return "Product type successfully removed!\n";
	}

}
