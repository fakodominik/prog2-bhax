package hu.unideb.prog2.webshop.command.impl;

import java.util.ArrayList;

import hu.unideb.prog2.webshop.command.AbstractCommand;
import hu.unideb.prog2.webshop.data.DataBase;
import hu.unideb.prog2.webshop.model.ProductType;

public class ListProductTypeCommand extends AbstractCommand{

	public ListProductTypeCommand(String userType, String entityType, String action) {
		super(userType, entityType, action);
	}

	@Override
	public String process(DataBase database) {
		String result = "";
		ArrayList<ProductType> array = new ArrayList<>(database.getProductTypeList());
		for(int i=0;i<array.size();++i)
		{
			result += array.get(i).toString() + "\n";
		}
		return result;
	}

	@Override
	public String process(DataBase database, String[] parameters) {
		return "No need for parameters!";
	}

}
