package hu.unideb.prog2.webshop.command.user.impl;

import hu.unideb.prog2.webshop.command.AbstractCommand;
import hu.unideb.prog2.webshop.data.DataBase;
import hu.unideb.prog2.webshop.model.Cart;
import hu.unideb.prog2.webshop.model.Product;

public class AddToCartUserCommand extends AbstractCommand{

	public AddToCartUserCommand(String userType, String entityType, String action) {
		super(userType, entityType, action);
	}

	@Override
	public String process(DataBase database) {
		return "Need parameters!";
	}

	@Override
	public String process(DataBase database, String[] parameters) {
		Product product = database.getProduct(Integer.parseInt(parameters[0]));
		if(product == null)
		{
			throw new IllegalArgumentException("No Product found with that id!");
		}
		Cart.addToCart(product);
		return "Item has been successfully added to the cart!\n";
	}

}
