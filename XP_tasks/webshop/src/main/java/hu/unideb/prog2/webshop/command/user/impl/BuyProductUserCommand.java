package hu.unideb.prog2.webshop.command.user.impl;

import java.util.ArrayList;

import hu.unideb.prog2.webshop.command.AbstractCommand;
import hu.unideb.prog2.webshop.data.DataBase;
import hu.unideb.prog2.webshop.model.Cart;
import hu.unideb.prog2.webshop.model.Product;

public class BuyProductUserCommand extends AbstractCommand{

	public BuyProductUserCommand(String userType, String entityType, String action) {
		super(userType, entityType, action);
	}

	@Override
	public String process(DataBase database) {
		
		ArrayList<Product> cart = Cart.getCart();
		if(!cart.isEmpty())
			{
			String result = Cart.listItems();
			long sum = 0;
			for(Product p : cart)
			{
				sum += p.getProductType().getPrice();
				database.removeProduct(p.getId());
			}
			Cart.emptyCart();
			return "You bought the following items : \n" + result + "Total : " + sum + "\n";
		}else
		{
			return "The cart was emtpy!\n";
		}
	}

	@Override
	public String process(DataBase database, String[] parameters) {
		return "No need for parameters!";
	}

}
