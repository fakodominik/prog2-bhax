package hu.unideb.prog2.webshop.command.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import hu.unideb.prog2.webshop.command.AbstractCommand;
import hu.unideb.prog2.webshop.data.DataBase;
import hu.unideb.prog2.webshop.model.Product;
import hu.unideb.prog2.webshop.model.ProductType;

public class ImportProductCommand extends AbstractCommand{

	public ImportProductCommand(String userType, String entityType, String action) {
		super(userType, entityType, action);
	}

	@Override
	public String process(DataBase database) {
		return "Need file name as parameter!";
	}

	@Override
	public String process(DataBase database, String[] parameters) {
		File file = new File(parameters[0]);
		Scanner scanner;
		try
		{
			scanner = new Scanner(file);
		}catch(FileNotFoundException e)
		{
			throw new IllegalArgumentException("File not found!");
		}
		
		String line;
		String[] parts;
		ArrayList<Product> productList = new ArrayList<>(database.getProductList());
		ArrayList<ProductType> productTypeList = new ArrayList<>(database.getProductTypeList());
		boolean alreadyInProduct = false;
		while(scanner.hasNextLine())
		{
			line = scanner.nextLine();
			parts = line.split(",");
			ProductType tempProductType = new ProductType(parts[0],parts[1],parts[2],Long.parseLong(parts[3]));
			if(productTypeList.size()!=0)
			{
				for(ProductType pt : productTypeList)
				{
					if(pt.equals(tempProductType))
					{
						Product tempProduct = new Product(tempProductType,Long.parseLong(parts[4]));
						if(productList.size()!=0)
						{
							for(Product p : productList)
							{
								if(p.equals(tempProduct))
								{
									alreadyInProduct = true;
								}
							}
							if(!alreadyInProduct)
							{
								database.addProduct(tempProduct, tempProduct.getId());
							}
						}else
						{
							database.addProduct(tempProduct, tempProduct.getId());
						}
					}else
					{
						database.addProductType(tempProductType);
						Product tempProduct = new Product(tempProductType,Long.parseLong(parts[4]));
						database.addProduct(tempProduct, tempProduct.getId());
					}
				}
			}else
			{
				database.addProductType(tempProductType);
				Product tempProduct = new Product(tempProductType,Long.parseLong(parts[4]));
				database.addProduct(tempProduct, tempProduct.getId());
			}
		}
		
		scanner.close();
		return "Products from : " + parameters[0] + " were successfully imported to database!\n";
	}

}
