package hu.unideb.prog2.webshop;

import hu.unideb.prog2.webshop.command.CommandBuilder;
import hu.unideb.prog2.webshop.controller.UserHandler;
import hu.unideb.prog2.webshop.data.DataBase;

public class Main {
	
	private static  DataBase database;
	private static UserHandler userHandler;

	public static void main(String[] args) throws Exception {
		
		//Avoid creating a new database with new userHandler upon catching any exception
		database = new DataBase();
		userHandler = new UserHandler(System.in,System.out,CommandBuilder.buildCommands(),database);
		
		
		//To run the program till the user writes "exit"
		try
		{
			startConsole();
		}catch(Exception e)
		{
			System.out.println(e.toString());
			startConsole();
		}
		
	}
	
	public static void startConsole() throws Exception
	{
		userHandler.process();
		userHandler.close();
	}

}
