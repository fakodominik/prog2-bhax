package hu.unideb.prog2.webshop.command.impl;

import hu.unideb.prog2.webshop.command.AbstractCommand;
import hu.unideb.prog2.webshop.data.DataBase;

public class RemoveProductCommand extends AbstractCommand{

	public RemoveProductCommand(String userType, String entityType, String action) {
		super(userType, entityType, action);
	}

	@Override
	public String process(DataBase database) {
		return "Need parameters!";
	}

	@Override
	public String process(DataBase database, String[] parameters) {
		database.removeProduct(Integer.parseInt(parameters[0]));
		return "Product successfully removed!\n";
	}

}
