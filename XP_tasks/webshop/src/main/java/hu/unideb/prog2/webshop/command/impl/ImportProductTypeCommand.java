package hu.unideb.prog2.webshop.command.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import hu.unideb.prog2.webshop.command.AbstractCommand;
import hu.unideb.prog2.webshop.data.DataBase;
import hu.unideb.prog2.webshop.model.ProductType;

public class ImportProductTypeCommand extends AbstractCommand{

	public ImportProductTypeCommand(String userType, String entityType, String action) {
		super(userType, entityType, action);
	}

	@Override
	public String process(DataBase database) {
		return "Need file name as parameter!";
	}

	@Override
	public String process(DataBase database, String[] parameters) {
		File  file = new File(parameters[0]);
		Scanner scanner;
		try
		{
			scanner = new Scanner(file);
		}catch(FileNotFoundException e)
		{
			throw new IllegalArgumentException("No file was found");
		}
		String line = "";
		String[] parts;
		ArrayList<ProductType> array = new ArrayList<>(database.getProductTypeList());
		boolean alreadyInDatabase = false;
		
		
		while(scanner.hasNextLine())
		{
			line = scanner.nextLine();
			parts = line.split(",");
			ProductType temp = new ProductType(parts[0],parts[1],parts[2],Long.parseLong(parts[3]));
			if(array.size() != 0)
			{
				for(ProductType p : array)
				{
					if(p.equals(temp))
					{
						alreadyInDatabase = true;
					}
				}
				if(!alreadyInDatabase)
				{
				database.addProductType(temp);
				
				}
			}else
			{
				database.addProductType(temp);
			}
			alreadyInDatabase = false;
		}
		
		scanner.close();
		return "ProductTypes from " + parameters[0] + " were successfully imported to database!\n";
	}

}
