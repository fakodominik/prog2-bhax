package hu.unideb.prog2.model;

public class Node {
	private Node rightChild;
	private Node leftChild;
	private int value;
	
	public Node(int value)
	{
		this.value = value;
		rightChild = null;
		leftChild = null;
	}
	public void setRightChild(Node node)
	{
		this.rightChild = node;
	}
	public void setLeftChild(Node node)
	{
		this.leftChild = node;
	}
	public int getValue()
	{
		return this.value;
	}
	public Node getRightChild() {
		return rightChild;
	}
	public Node getLeftChild() {
		return leftChild;
	}
}
