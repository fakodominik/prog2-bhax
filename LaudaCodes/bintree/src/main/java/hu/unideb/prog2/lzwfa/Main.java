package hu.unideb.prog2.lzwfa;

import java.util.Random;

import hu.unideb.prog2.calculation.AverageValue;
import hu.unideb.prog2.calculation.Deviaton;
import hu.unideb.prog2.model.BinaryTree;

public class Main {

	public static void main(String[] args) {
		BinaryTree binfa = new BinaryTree();
		Random rand = new Random();
		for(int i=0;i<10;i++)
		{
			int a = rand.nextInt(100);
			binfa.add(a);
			System.out.println("Added to the tree : " + a);
		}
		binfa.print();
		System.out.println("Avg: " + AverageValue.getAvg(binfa));
		System.out.println("Deviation : " + Deviaton.getDeviation(AverageValue.getAvg(binfa),binfa));
		
	}

}
