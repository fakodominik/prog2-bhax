package aspectj_runtime;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

	private static long NUMBER_OF_VALUES = 10000000;
	private static 	Random rand = new Random();
	
	public static void main(String[] args) {

		ArrayList<Double> list = (ArrayList<Double>) addToList();
	}
	
	private static List<Double> addToList()
	{
		List<Double> list = new ArrayList<>();
		
		for(int i = 0; i < NUMBER_OF_VALUES; i++)
		{
			list.add(rand.nextDouble());
		}
		
		return list;
	}

}
