package spring_runtime;

import java.util.ArrayList;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	private static String PATH_TO_XML = "springConfig.xml";
	
	public static void main(String[] args) {

		ApplicationContext ctx = new ClassPathXmlApplicationContext(PATH_TO_XML);
		Counter counter = ctx.getBean("counter",Counter.class);
		
		ArrayList<Double> list = (ArrayList<Double>) counter.addToList();
	}
	
	

}
