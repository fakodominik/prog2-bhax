package spring_runtime;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Counter {

	private static long NUMBER_OF_VALUES = 1000000;
	private static Random RAND = new Random();
	private long start;
	private long end;
	
	public void start()
	{
		start = System.currentTimeMillis();
	}
	
	public void end()
	{
		end = System.currentTimeMillis();
		System.out.println("Time spent during the method : " + (end-start) + "ms");
	}
	
	public List<Double> addToList()
	{
		List<Double> list = new ArrayList<>();
		
		for(int i = 0; i < NUMBER_OF_VALUES; i++)
		{
			list.add(RAND.nextDouble());
		}
		
		return list;
	}
	
	
}
