package Car;


/**
* @generated
*/
public class Car implements Vehicle {
    
    /**
    * @generated
    */
    private Integer numberOfTires;
    
    /**
    * @generated
    */
    private String nameOFTheOwner;
    
    /**
    * @generated
    */
    private String registrationNumber;
    
    /**
    * @generated
    */
    private String model;
    
    /**
    * @generated
    */
    private Date dateOfProducion;
    
    /**
    * @generated
    */
    private DataType get Car;
    
    /**
    * @generated
    */
    private invalid attribute;
    
    
    
    /**
    * @generated
    */
    protected Integer getNumberOfTires() {
        return this.numberOfTires;
    }
    
    /**
    * @generated
    */
    protected Integer setNumberOfTires(Integer numberOfTires) {
        this.numberOfTires = numberOfTires;
    }
    
    /**
    * @generated
    */
    protected String getNameOFTheOwner() {
        return this.nameOFTheOwner;
    }
    
    /**
    * @generated
    */
    protected String setNameOFTheOwner(String nameOFTheOwner) {
        this.nameOFTheOwner = nameOFTheOwner;
    }
    
    /**
    * @generated
    */
    protected String getRegistrationNumber() {
        return this.registrationNumber;
    }
    
    /**
    * @generated
    */
    protected String setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }
    
    /**
    * @generated
    */
    protected String getModel() {
        return this.model;
    }
    
    /**
    * @generated
    */
    protected String setModel(String model) {
        this.model = model;
    }
    
    /**
    * @generated
    */
    protected Date getDateOfProducion() {
        return this.dateOfProducion;
    }
    
    /**
    * @generated
    */
    protected Date setDateOfProducion(Date dateOfProducion) {
        this.dateOfProducion = dateOfProducion;
    }
    
    /**
    * @generated
    */
    public DataType getGet Car() {
        return this.get Car;
    }
    
    /**
    * @generated
    */
    public DataType setGet Car(DataType get Car) {
        this.get Car = get Car;
    }
    
    /**
    * @generated
    */
    public invalid getAttribute() {
        return this.attribute;
    }
    
    /**
    * @generated
    */
    public invalid setAttribute(invalid attribute) {
        this.attribute = attribute;
    }
    
    

    //                          Operations                                  
    
    /**
    * @generated
    */
    public getCar() {
        //TODO
    }
    /**
    * @generated
    */
    public () {
        //TODO
    }
    
}
