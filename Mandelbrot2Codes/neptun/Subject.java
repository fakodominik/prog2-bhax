package neptun;

public abstract class Subject {
	protected String name;
	
	public String getName()
	{
		return this.name;
	}
}
