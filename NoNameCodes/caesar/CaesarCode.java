package caesar;

import java.util.Map;

public class CaesarCode {

	private static Map<Integer,String> asciiCodes;
	
	public static String code(char[] chars)
	{
		String result = "";
		int key = 0;
		
		for(Character c : chars)
		{
			if(!asciiCodes.containsValue(c.toString()))
			{
				throw new RuntimeException("No code found for this character : " + c);
			}
			for(Map.Entry<Integer,String> entry : asciiCodes.entrySet())
			{
				if(entry.getValue().equals(c.toString()))
				{
					if(entry.getKey() + 3 > asciiCodes.size())
					{
						key = Math.abs(asciiCodes.size() - entry.getKey());
					}else
					{
						key = entry.getKey() + 3;
					}
				}
			}
			result += asciiCodes.get(key);
		}
		
		return result;
	}
	
	public static void setCodes() throws Exception
	{
		asciiCodes = Import.getCodes();
	}
	
}
