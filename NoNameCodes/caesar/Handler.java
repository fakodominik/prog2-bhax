package caesar;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class Handler implements AutoCloseable{

	private Scanner input;
	private FileWriter output;
	
	public Handler(InputStream input, String output) throws IOException
	{
		this.input = new Scanner(input);
		this.output = new FileWriter(output);
	}
	
	public void process() throws IOException
	{
		String line;
		char[] chars;
		String coded;
		
		while(!(line = input.nextLine()).equals("") )
		{
			chars = line.toCharArray();
			coded = CaesarCode.code(chars);
			Output.write(output,coded);
		}
	}

	@Override
	public void close() throws Exception {
		input.close();
		output.close();
	}
	
}
