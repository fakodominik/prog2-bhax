package hu.unideb.prog2.xml;

import java.awt.Color;
import java.awt.Graphics2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;

import hu.unideb.prog2.model.City;

public class SvgMaker {

	private static  SVGGraphics2D svgGenerator;
	
	public static void make(ArrayList<City> cities,String output) throws IOException {

	   //Get the different colors
	   Map<String,Color> colors = ColorCreator.getColors(cities);
	   
	   //Get an SVG Doc
       DOMImplementation domImpl = GenericDOMImplementation.getDOMImplementation();
       String svgNS = "http://www.w3.org/2000/svg";
       Document document = domImpl.createDocument(svgNS, "svg", null);

       svgGenerator = new SVGGraphics2D(document);
       
       //Draw points
      cities.forEach(city ->
      {
    	paint(svgGenerator,city,colors.get(city.getState()));
      }	  
    		  );
      //Save to SVG
       svgGenerator.stream(output,true);
       
   
    }
	
	private static void paint(Graphics2D svgGenerator, City city,Color color)
	{
		svgGenerator.setPaint(color);

		svgGenerator.fillOval(Integer.parseInt(city.getCoordinateX()), Integer.parseInt(city.getCoordinateY())
				              , 10, 10);
		
	}

}

