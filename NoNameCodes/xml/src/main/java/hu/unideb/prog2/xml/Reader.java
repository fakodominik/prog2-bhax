package hu.unideb.prog2.xml;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import hu.unideb.prog2.model.City;

public class Reader {
	
	private File input;
	private ArrayList<City> cities;
	
	public Reader(String in)
	{
		this.input = new File(in);
		this.cities = new ArrayList<City>();
	}
	
	public ArrayList<City> read() throws ParserConfigurationException, SAXException, IOException
	{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();  
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(input);
		doc.getDocumentElement().normalize(); 
		
		NodeList nodeList = doc.getElementsByTagName("city"); 
		
		for (int i = 0; i < nodeList.getLength(); i++)   
		{  
			Node node = nodeList.item(i);   
			if (node.getNodeType() == Node.ELEMENT_NODE)   
			{  
				Element element = (Element) node;  
				City temp = new City(
						element.getElementsByTagName("coordinateX").item(0).getTextContent(),
						element.getElementsByTagName("coordinateY").item(0).getTextContent(),
						element.getElementsByTagName("state").item(0).getTextContent()
						);
				cities.add(temp);
			}	
		}  
		return cities;
	}
}
