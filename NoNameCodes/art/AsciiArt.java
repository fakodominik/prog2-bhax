package art;

public class AsciiArt {
	
	public static void main(String[] args) throws Exception
	{
		InputHandler inputHandler = new InputHandler(args[0],args[1]);
		
		inputHandler.process();
		inputHandler.close();
	}
}
