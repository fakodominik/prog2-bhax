package gone;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class BugousStuffProducer{
	private final Writer writer;

	public BugousStuffProducer(String outputFileName) throws
	IOException {
		writer = new FileWriter(outputFileName);
	}

	public void writeStuff() throws IOException {
		writer.write("Stuff");
		writer.close();
	}

	@Override
	public void finalize() throws IOException {
		writer.close();
	}	
}