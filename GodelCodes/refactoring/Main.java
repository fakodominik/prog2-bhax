package refactoring;

import java.util.Arrays;

public class Main {

	static String str = "";

	public static void main(String[] args) {
		String[] strings = {"Hello,", " this", " is", " the", " sample", " text!"};
		/* Without lambda
		String str = OldStringBuilder.builder(strings);
		*/
		
		//With lambda
		StringBuilder strBuilder = (s) -> {return s;};
		
		Arrays.stream(strings).forEach(s -> str += strBuilder.addToString(s));

		System.out.println(str);
	}
}
