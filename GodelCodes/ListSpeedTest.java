import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

public class ListSpeedTest
{
	private static long maxNum = 1000000;

	public static void main(String[] args)
	{
		ArrayListMeasure();
		System.out.println("");
		LinkedListMeasure();

	}
	public static void ArrayListMeasure()
	{
		ArrayList<Integer> arrayList = new ArrayList<>();
		long start = System.currentTimeMillis();
		for(int i= 0;i<maxNum;i++)
		{
			arrayList.add(i);
		}
		long finish = System.currentTimeMillis();
		long elapsedTime = finish - start;

		System.out.println("Adding elements to ArrayList" + 
			" while not passing the count of numbers : " + elapsedTime);

		ArrayList<Integer> arrayListNew = new ArrayList<Integer>((int)maxNum);
		start = System.currentTimeMillis();
		for(int i= 0;i<maxNum;i++)
		{
			arrayListNew.add(i);
		}
		finish = System.currentTimeMillis();
		elapsedTime = finish - start;
		System.out.println("Adding elements to ArrayList" + 
			" while passing the count of numbers : " + elapsedTime);

		start = System.currentTimeMillis(); 
		for(int i = 0;i<maxNum;i++)
		{
			Collections.sort(arrayList);
		}
		finish = System.currentTimeMillis();
		elapsedTime = finish - start;
		System.out.println("Sorting elements in ArrayList : " + elapsedTime);

	}
	public static void LinkedListMeasure()
	{
		LinkedList<Integer> linkedList = new LinkedList<>();
		long start = System.currentTimeMillis();
		for(int i= 0;i<maxNum;i++)
		{
			linkedList.add(i);
		}
		long finish = System.currentTimeMillis();
		long elapsedTime = finish - start;

		System.out.println("Adding elements to LinkedList : " + elapsedTime);
		
		start = System.currentTimeMillis(); 
		for(int i = 0;i<maxNum;i++)
		{
			Collections.sort(linkedList);
		}
		finish = System.currentTimeMillis();
		elapsedTime = finish - start;
		System.out.println("Sorting elements in LinkedList : " + elapsedTime);

	}
}